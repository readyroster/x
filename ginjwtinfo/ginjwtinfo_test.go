package ginjwtinfo

import "testing"

const testJSONOne = "{ \"lvlone\": { \"lvltwo\": {\"text\": \"bob\"} } }"

func TestGetJSONInfo(t *testing.T) {
	value, err := getJSONInfo("lvlone.lvltwo.text", []byte(testJSONOne))
	if err != nil {
		t.Error(err)
	}

	valStr, isStr := value.(string)

	if !isStr {
		t.Errorf("value was not of type string")
	}

	if valStr != "bob" {
		t.Errorf("value was %s rather then expected bob", valStr)
	}
}
