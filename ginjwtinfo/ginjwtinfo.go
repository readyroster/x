package ginjwtinfo

import (
	"encoding/json"
	"errors"
	"strings"

	"github.com/gin-gonic/gin"
)

//AddJWTInfoToContext looks in Auth_Plain using the jsonSearch provided and sets what ever it finds in the context under ctxName
//jsonSearch uses the simple format of levelone.leveltwo.object in this instance it would return what ever is found in object
//so in levelone{leveltwo{object: "test"}} it would return test
func AddJWTInfoToContext(ctxName, jsonSearch string) gin.HandlerFunc {
	return func(c *gin.Context) {
		auth, authFound := c.Get("Auth_Plain")
		if !authFound {
			c.AbortWithError(500, errors.New("Auth not found"))
			return
		}

		value, err := getJSONInfo(jsonSearch, auth.([]byte))
		if err != nil {
			c.AbortWithError(500, err)
			return
		}

		c.Set(ctxName, value)
	}
}

//getJsonInfo uses a simple search format(searh.for.me will return the object me)
func getJSONInfo(searchTerm string, jsonRaw []byte) (interface{}, error) {
	searchTerms := strings.Split(searchTerm, ".")

	var mapObj map[string]interface{}
	err := json.Unmarshal(jsonRaw, &mapObj)
	//mapObj, ok := json.(map[string]interface{})
	var obj interface{}
	if err != nil {
		return nil, err
	}
	for i := 0; i < len(searchTerms); i++ {
		if i == len(searchTerms)-1 {
			obj = mapObj[searchTerms[i]].(interface{})
			continue
		}
		mapObj = mapObj[searchTerms[i]].(map[string]interface{})
	}

	return obj, nil
}
