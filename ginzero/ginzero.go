package ginzero

import (
	"context"
	"net"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/rs/xid"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

// FromRequest gets the logger in the request's context.
// This is a shortcut for log.Ctx(r.Context())
func FromRequest(r *http.Request) *zerolog.Logger {
	return log.Ctx(r.Context())
}

// NewHandler injects log into requests context.
func NewHandler(log zerolog.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		l := log.With().Logger()
		c.Request.WithContext(l.WithContext(c.Request.Context()))
		c.Next()
	}
}

// URLHandler adds the requested URL as a field to the context's logger
// using fieldKey as field key.
func URLHandler(fieldKey string) gin.HandlerFunc {
	return func(c *gin.Context) {
		log := zerolog.Ctx(c.Request.Context())
		log.UpdateContext(func(con zerolog.Context) zerolog.Context {
			return con.Str(fieldKey, c.Request.URL.String())
		})
		c.Next()
	}
}

// MethodHandler adds the request method as a field to the context's logger
// using fieldKey as field key.
func MethodHandler(fieldKey string) gin.HandlerFunc {
	return func(c *gin.Context) {
		log := zerolog.Ctx(c.Request.Context())
		log.UpdateContext(func(con zerolog.Context) zerolog.Context {
			return con.Str(fieldKey, c.Request.Method)
		})
		c.Next()
	}
}

// RequestHandler adds the request method and URL as a field to the context's logger
// using fieldKey as field key.
func RequestHandler(fieldKey string) gin.HandlerFunc {
	return func(c *gin.Context) {
		log := zerolog.Ctx(c.Request.Context())
		log.UpdateContext(func(con zerolog.Context) zerolog.Context {
			return con.Str(fieldKey, c.Request.Method+" "+c.Request.URL.String())
		})
		c.Next()
	}
}

// RemoteAddrHandler adds the request's remote address as a field to the context's logger
// using fieldKey as field key.
func RemoteAddrHandler(fieldKey string) gin.HandlerFunc {
	return func(c *gin.Context) {
		if host, _, err := net.SplitHostPort(c.Request.RemoteAddr); err == nil {
			log := zerolog.Ctx(c.Request.Context())
			log.UpdateContext(func(c zerolog.Context) zerolog.Context {
				return c.Str(fieldKey, host)
			})
		}
		c.Next()
	}
}

// UserAgentHandler adds the request's user-agent as a field to the context's logger
// using fieldKey as field key.
func UserAgentHandler(fieldKey string) gin.HandlerFunc {
	return func(c *gin.Context) {
		if ua := c.Request.Header.Get("User-Agent"); ua != "" {
			log := zerolog.Ctx(c.Request.Context())
			log.UpdateContext(func(con zerolog.Context) zerolog.Context {
				return con.Str(fieldKey, ua)
			})
		}
		c.Next()
	}
}

// RefererHandler adds the request's referer as a field to the context's logger
// using fieldKey as field key.
func RefererHandler(fieldKey string) gin.HandlerFunc {
	return func(c *gin.Context) {
		if ref := c.Request.Header.Get("Referer"); ref != "" {
			log := zerolog.Ctx(c.Request.Context())
			log.UpdateContext(func(c zerolog.Context) zerolog.Context {
				return c.Str(fieldKey, ref)
			})
		}
		c.Next()
	}
}

type idKey struct{}

// IDFromRequest returns the unique id associated to the request if any.
func IDFromRequest(r *http.Request) (id xid.ID, ok bool) {
	if r == nil {
		return
	}
	return IDFromCtx(r.Context())
}

// IDFromCtx returns the unique id associated to the context if any.
func IDFromCtx(ctx context.Context) (id xid.ID, ok bool) {
	id, ok = ctx.Value(idKey{}).(xid.ID)
	return
}

// RequestIDHandler returns a handler setting a unique id to the request which can
// be gathered using IDFromRequest(req). This generated id is added as a field to the
// logger using the passed fieldKey as field name. The id is also added as a response
// header if the headerName is not empty.
//
// The generated id is a URL safe base64 encoded mongo object-id-like unique id.
// Mongo unique id generation algorithm has been selected as a trade-off between
// size and ease of use: UUID is less space efficient and snowflake requires machine
// configuration.
func RequestIDHandler(fieldKey, headerName string) gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := c.Request.Context()
		id, ok := IDFromRequest(c.Request)
		if !ok {
			id = xid.New()
			ctx = context.WithValue(ctx, idKey{}, id)
			c.Request.WithContext(ctx)
		}
		if fieldKey != "" {
			log := zerolog.Ctx(ctx)
			log.UpdateContext(func(c zerolog.Context) zerolog.Context {
				return c.Str(fieldKey, id.String())
			})
		}
		if headerName != "" {
			c.Header(headerName, id.String())
		}
		c.Next()
	}
}

// CustomHeaderHandler adds given header from request's header as a field to
// the context's logger using fieldKey as field key.
func CustomHeaderHandler(fieldKey, header string) gin.HandlerFunc {
	return func(c *gin.Context) {
		if val := c.Request.Header.Get(header); val != "" {
			log := zerolog.Ctx(c.Request.Context())
			log.UpdateContext(func(c zerolog.Context) zerolog.Context {
				return c.Str(fieldKey, val)
			})
		}
		c.Next()
	}
}
