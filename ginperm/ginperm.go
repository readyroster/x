package ginperm

import (
	"github.com/gin-gonic/gin"
)

type Permission struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}

type Permissions struct {
	Permissions []Permission `json:"permissions"`
}

func NewPermissions() Permissions {
	return Permissions{Permissions: make([]Permission, 0, 5)}
}

func (p *Permissions) AddPermission(name, description string) {
	p.Permissions = append(p.Permissions, Permission{Name: name, Description: description})
}

func (p *Permissions) RegisterHandler(engine *gin.Engine) {
	engine.GET("/well-known/permissions.json", p.JSONHandler)
}

func (p *Permissions) JSONHandler(c *gin.Context) {
	c.JSON(200, p)
}
