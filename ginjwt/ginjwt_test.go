package ginjwt

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
)

const jwk = `
    {
      "kid": "Uf0Ue_vO5uQy-jJrvrWdggNvZRnf8zou5oSM1vgY4Uc",
      "kty": "RSA",
      "alg": "RS256",
      "use": "sig",
      "n": "oXfRQjqCZgQpazabmq2Vlr-oUH3qUVAnM5DDXQAPHxHUAW7Sy7-xuKPevccG2zmmnvUhXlx4qXN6908lmFo_ZWooPGY1A4IQ5yEITa8ayTOtvRNs32sFw2J2GPuJ_YSm8R8z8tb4bK5biM3RE1_0Wb5Oeu79zjWrFodE0_0M7ba4CgjIOmFycYbHABaDYZTTkcKkwdKAbImFP_tWb_3EvphNzwika-OcZHKQkeGF-64-5MP2rQeCpsXGOa0L1j_oHlRixqm9HUqNFsLb4kWOUT9Xm9a5hiS4Td9WrJdbRWZIEutP6yXIfiQqc62FDzTzo_dJgyIqlj7TaTfXzEvi2w",
      "e": "AQAB",
      "x5c": [
        "MIICpTCCAY0CBgFxLK55uDANBgkqhkiG9w0BAQsFADAWMRQwEgYDVQQDDAtSZWFkeXJvc3RlcjAeFw0yMDAzMzAxODIxMDRaFw0zMDAzMzAxODIyNDRaMBYxFDASBgNVBAMMC1JlYWR5cm9zdGVyMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoXfRQjqCZgQpazabmq2Vlr+oUH3qUVAnM5DDXQAPHxHUAW7Sy7+xuKPevccG2zmmnvUhXlx4qXN6908lmFo/ZWooPGY1A4IQ5yEITa8ayTOtvRNs32sFw2J2GPuJ/YSm8R8z8tb4bK5biM3RE1/0Wb5Oeu79zjWrFodE0/0M7ba4CgjIOmFycYbHABaDYZTTkcKkwdKAbImFP/tWb/3EvphNzwika+OcZHKQkeGF+64+5MP2rQeCpsXGOa0L1j/oHlRixqm9HUqNFsLb4kWOUT9Xm9a5hiS4Td9WrJdbRWZIEutP6yXIfiQqc62FDzTzo/dJgyIqlj7TaTfXzEvi2wIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQA6dPoUT5BStzvJzP4tPyTxSxHR4cIVwMGdYcbLUjMvoWu6IN4Kn/8ZXJycHjeDn3Vm6Uw7YuZrOAy43Sdu6dUsDib4LRppzmyJ27ZkB1pM/JcCWKLjC+NcMmuD39ZuAKuuIopUBEMBAEiIbGpuRcce9KgX1HXA0fiom43eQvfTFxk3e8hEtTnKy0lJIPzzuJURpVwQOWttcuQb+/2AckaqNu0zftjuwRf1IoX+GEYXiNPOCwWuo2zI4Cv+CLwi8yjQhpy42mYZbiFCwBmvlMO9Ch+b98BFEOzlTuOUo72T8tv1xth1wZrb2CMiLNQpxsZL0CIyc9U6aHDvLwlzT4Re"
      ],
      "x5t": "6h_gUMagIDH5S3_J97WJizVWJMs",
      "x5t#S256": "ereDh42FXKroNw3xxQ33c680Rz-XHBKh5w4o3z51cQg"
    }`

const jwt = `eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJVZjBVZV92TzV1UXktakpydnJXZGdnTnZaUm5mOHpvdTVvU00xdmdZNFVjIn0.eyJleHAiOjE1ODU1OTQ3OTAsImlhdCI6MTU4NTU5NDQ5MCwiYXV0aF90aW1lIjoxNTg1NTk0NDg5LCJqdGkiOiJkZDBkMGJlNy1jYWY1LTRmOTktODIxMC00Zjk3MGQ2YzY3ZGQiLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvYXV0aC9yZWFsbXMvUmVhZHlyb3N0ZXIiLCJhdWQiOiJhY2NvdW50Iiwic3ViIjoiZmQwNDc2ZjgtMGNmNS00OWQ3LTg0NTUtMWU5OGZkNDIxMTZlIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoicmVhZHktcm9zdGVyLWNsaWVudCIsInNlc3Npb25fc3RhdGUiOiJjMzI0NmQyNy1jMjcxLTQxZjEtYTZkMy1iZjg1OTI3NGQ4MjgiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbImh0dHA6Ly9sb2NhbGhvc3Q6OTEwMCJdLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsib2ZmbGluZV9hY2Nlc3MiLCJ1bWFfYXV0aG9yaXphdGlvbiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sInNjb3BlIjoib3BlbmlkIHByb2ZpbGUgZW1haWwgYnVzaW5lc3NpZCIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwibmFtZSI6ImpvaG4gZ3JheSIsInByZWZlcnJlZF91c2VybmFtZSI6ImpvaG5mZzEwIiwiZ2l2ZW5fbmFtZSI6ImpvaG4iLCJmYW1pbHlfbmFtZSI6ImdyYXkiLCJlbWFpbCI6ImpvaG5mZzI2MTBAZ21haWwuY29tIn0.C9HL362KsurG8aZZtE4tDEAsG0aNMpiocXB4x3TxPYkqiuhS0mt4vZtl61S8yJswFPP7COFEXWaE0MPSmuyIfJFpsCHFVxX4dt1Vk6iZEEuW9SuZFjKbOD9p42VGhGXx0lHnhokES9xahDSye4LtXTiQHZI38rcRxlPq5fYkCc_odmTJ_Hi9poNHidZ5yyH-Dxh8dCkBmtM0-bd_r46nkmL_m1p2FZz3-ukIgLuDrA385LI8jzIA4pqBxXlcnRByV9JYyp6vimOuL7zd_OXQslC5du4neKcMuJeouGiYKj0E2OqfPNEIM7MV6s9Tzeevi6xqxmEROXAbm-uH-A4xaw`

func setupRouter() *gin.Engine {
	r := gin.Default()
	r.GET("/ping", func(c *gin.Context) {
		c.String(200, "pong")
	})
	return r
}
func TestLoadJSONWebKey(t *testing.T) {
	key, err := loadJSONWebKey([]byte(jwk))

	if err != nil {
		t.Error(err)
	}

	t.Log(key.Algorithm)
}

// func TestJWTHandler(t *testing.T) {
// 	responseR := httptest.NewRecorder()
// 	ctx, engine := gin.CreateTestContext(responseR)
// 	engine.Use()
// 	engine.Use(JWTHandler())
// }

func TestNewGinJWTHandleValidJWTToken(t *testing.T) {
	r := gin.Default()

	r.Use(NewGinJWTHandle(func() ([]byte, error) {
		return []byte(jwk), nil
	}, "Authorize"))

	r.GET("/ping", func(c *gin.Context) {
		c.String(200, "pong")
	})

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/ping", nil)
	req.Header.Add("Authorize", jwt)
	r.ServeHTTP(w, req)

	if w.Code != 200 {
		t.Error("Error wrong code returned", w.Code)
		return
	}

	// responseR := httptest.NewRecorder()
	// ctx, engine := gin.CreateTestContext(responseR)

}

func TestNewGinJWTHandleInvalidJWTToken(t *testing.T) {
	r := gin.Default()

	r.Use(NewGinJWTHandle(func() ([]byte, error) {
		return []byte(jwk), nil
	}, "Authorize"))

	//var ginJWT *GinJWT
	r.GET("/ping", func(c *gin.Context) {
		fmt.Println("Test")
		ginJWTGet, _ := c.Get("GinJWT")

		ginJWT := ginJWTGet.(*GinJWT)

		if ginJWT == nil {
			t.Error("GinJWT was nil")
			return
		}

		if ginJWT.Header != "Authorize" {
			t.Error("GinJWT header was not Authorize")
			return
		}

		fmt.Println(ginJWT.Header)

		c.String(200, "pong")
	})

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/ping", nil)
	req.Header.Add("Authorize", "timmy")
	r.ServeHTTP(w, req)

	if w.Code != 200 {
		t.Error("Error wrong code returned", w.Code)
		return
	}

	// responseR := httptest.NewRecorder()
	// ctx, engine := gin.CreateTestContext(responseR)

}

func TestJWSHandler(t *testing.T) {
	r := gin.Default()

	r.Use(NewGinJWTHandle(func() ([]byte, error) {
		return []byte(jwk), nil
	}, "Authorize"),
		JWSHandler())

	//var ginJWT *GinJWT
	r.GET("/ping", func(c *gin.Context) {

		jwtRaw, found := c.MustGet("JWT").(map[string]interface{})
		if !found {
			t.Error("JWT did not convert to map[string]interface")
			return
		}
		t.Log(jwtRaw)
		expRaw := jwtRaw["email"]

		t.Log(expRaw)
		// if !expRawExists {
		// 	t.Error("EXP does not exist in jwt")
		// 	return
		// }

		exp, expExists := expRaw.(string)
		if !expExists {
			t.Error("EXP does not contain uint")
			return
		}

		if exp != "johnfg2610@gmail.com" {
			t.Errorf("EXP does not match what it should be it was instead: %s", exp)
			return
		}

		c.String(200, "pong")
	})

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/ping", nil)
	req.Header.Add("Authorize", jwt)
	r.ServeHTTP(w, req)

	if w.Code != 200 {
		t.Error("Error wrong code returned", w.Code)
		return
	}
}
