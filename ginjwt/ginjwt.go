package ginjwt

import (
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"gopkg.in/square/go-jose.v2"
)

//JSONKeyFunc get a json key from a byte array
type JSONKeyFunc = func(bytes []byte) []byte

//GinJWT is used
type GinJWT struct {
	Header string
	JWK    *jose.JSONWebKey
}

// //NewGinJWT creates a new GinJWT instance from a url
// func NewGinJWT(url string, header string) (*GinJWT, error) {
// 	resp, err := http.Get(url)
// 	if err != nil {
// 		return nil, err
// 	}
// 	var body []byte
// 	_, err = resp.Body.Read(body)
// 	if err != nil {
// 		return nil, err
// 	}

// 	jsonwebkey, err := loadJSONWebKey(body)
// 	if err != nil {
// 		return nil, err
// 	}
// 	jwtMiddleware := GinJWT{JWK: jsonwebkey, Header: header}

// 	return &jwtMiddleware, nil
// }

func loadJSONWebKey(json []byte) (*jose.JSONWebKey, error) {
	var jwk jose.JSONWebKey

	err := jwk.UnmarshalJSON(json)
	if err != nil {
		return nil, err
	}
	if !jwk.Valid() {
		return nil, errors.New("invalid JWK key")
	}
	return &jwk, nil
}

// // JWTHandler handles JWT authentication
// func (ginJWT *GinJWT) JWTHandler() gin.HandlerFunc {
// 	return func(c *gin.Context) {
// 		obj, err := jose.ParseSigned(c.GetHeader(ginJWT.Header))
// 		if err != nil {
// 			c.Error(err)
// 		}
// 		plain, err := obj.Verify(ginJWT.JWK)
// 		if err != nil {
// 			c.Error(err)
// 		}
// 		c.Set("Authorisation_Plain", plain)
// 		c.Next()
// 	}
// }

//NewJSONKeyFunc will simply return the bytearr
func NewJSONKeyFunc(bytes []byte) []byte {
	return bytes
}

//GetJWKFunc returns a JWK
type GetJWKFunc = func() ([]byte, error)

//NewGetJWKFuncFromURL z
func NewGetJWKFuncFromURL(url string) ([]byte, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	var body []byte
	_, err = resp.Body.Read(body)
	if err != nil {
		return nil, err
	}

	return body, nil
}

//NewGinJWTHandle adds a GinJWT instance to the context
func NewGinJWTHandle(jwkFunc GetJWKFunc, header string) gin.HandlerFunc {
	return func(c *gin.Context) {
		keyRaw, err := jwkFunc()
		if err != nil {
			c.AbortWithError(http.StatusUnauthorized, err)
			return
		}
		key, err := loadJSONWebKey(keyRaw)
		if err != nil {
			c.AbortWithError(http.StatusUnauthorized, err)
			return
		}

		c.Set("GinJWT", GinJWT{JWK: key, Header: header})

		c.Next()
	}
}

//GetGinJWT gets GinJWT from context
func GetGinJWT(c *gin.Context) (GinJWT, error) {
	ginJWT := c.MustGet("GinJWT").(GinJWT)
	return ginJWT, nil
	// ginJWTInterface, exists := c.Get("GinJWT")
	// if exists {
	// 	return nil, errors.New("GinJWT could not be found in context. please ensure you have installed the jwt handler")
	// }

	// ginJWT, isOk := ginJWTInterface.(GinJWT)
	// if !isOk {
	// 	return nil, errors.New("GinJWT not found in context")
	// }

	// return &ginJWT, nil
}

//GetJWSFromHeader gets a JWS from a header
func GetJWSFromHeader(header string, c *gin.Context) (*jose.JSONWebSignature, error) {
	hVal := c.GetHeader(header)
	var withouthBearer string
	if strings.Contains(hVal, "Bearer ") {
		withouthBearer = strings.ReplaceAll(hVal, "Bearer ", "")
	} else {
		withouthBearer = hVal
	}
	obj, err := jose.ParseSigned(withouthBearer)
	return obj, err
}

//JWSHandler puts a JWT in the context
func JWSHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		ginJWT, err := GetGinJWT(c)
		if err != nil {
			c.AbortWithError(http.StatusUnauthorized, err)
			return
		}
		fmt.Println(ginJWT.Header)

		jws, err := GetJWSFromHeader(ginJWT.Header, c)
		if err != nil {
			c.AbortWithError(http.StatusUnauthorized, err)
			return
		}
		plain, err := jws.Verify(ginJWT.JWK)
		if err != nil {
			c.AbortWithError(http.StatusUnauthorized, err)
			return
		}

		// var jwt interface{}
		// err = json.Unmarshal(plain, &jwt)
		// if err != nil {
		// 	c.Error(err)
		// 	c.AbortWithError(500, err)
		// 	return
		// }

		c.Set("JWT", plain)

		c.Next()
	}
}

//GetJWTFromContext gets the JWT from the current context
func GetJWTFromContext(c *gin.Context) ([]byte, error) {

	maps, mapsExist := c.MustGet("JWT").([]byte)
	if !mapsExist {
		return nil, errors.New("JWT did not exist in context")
	}

	return maps, nil
}

//JWTAuthroizeFunc authorizes a user to access a resource
type JWTAuthroizeFunc = func(jwt []byte) (bool, error)

//AuthorizeJWT authorize JWT using custom function
func AuthorizeJWT(jwtAuthFunc JWTAuthroizeFunc) gin.HandlerFunc {
	return func(c *gin.Context) {
		jwt, err := GetJWTFromContext(c)
		if err != nil {
			c.AbortWithError(http.StatusUnauthorized, err)
			return
		}

		authorized, err := jwtAuthFunc(jwt)

		if err != nil {
			c.AbortWithError(http.StatusUnauthorized, err)
			return
		}

		if authorized {
			c.Next()
		} else {
			c.AbortWithStatus(401)
			return
		}
	}
}

//VerifyJWTState verify expirytime and not before time
func VerifyJWTState() gin.HandlerFunc {
	return func(c *gin.Context) {
		jwt, err := GetJSONWebTokenFromContext(c)
		if err != nil {
			c.AbortWithError(http.StatusUnauthorized, err)
			return
		}

		if jwt.ExpiryTime > 0 {
			expiryTime := time.Unix(jwt.ExpiryTime, 0)
			expDuration := time.Until(expiryTime)
			if expDuration < 0 {
				c.AbortWithStatus(http.StatusUnauthorized)
				return
			}
		}

		if jwt.NotBefore > 0 {
			notBefore := time.Unix(jwt.NotBefore, 0)
			nbfDuration := time.Since(notBefore)
			if nbfDuration > 0 {
				c.AbortWithStatus(http.StatusUnauthorized)
				return
			}
		}

		c.Next()
	}

}

// //NewGinJWTHandler creates a new GinJWT instance from a url
// func NewGinJWTHandler(url string, jsonKeyFunc JSONKeyFunc, header string) gin.HandlerFunc {
// 	return func(c *gin.Context) {
// 		resp, err := http.Get(url)
// 		if err != nil {
// 			c.AbortWithError(500, err)
// 			return
// 		}
// 		var body []byte
// 		_, err = resp.Body.Read(body)
// 		if err != nil {
// 			c.AbortWithError(500, err)
// 			return
// 		}

// 		jsonwebkey, err := loadJSONWebKey(jsonKeyFunc(body))
// 		if err != nil {
// 			c.AbortWithError(500, err)
// 			return
// 		}
// 		c.Set("GinJWT", GinJWT{JWK: jsonwebkey, Header: header})

// 		c.Next()
// 	}

// }

// JWTHandler handles JWT authentication
// func JWTHandler() gin.HandlerFunc {
// 	return func(c *gin.Context) {
// 		ginJWTInterface, exists := c.Get("GinJWT")
// 		if exists {
// 			c.AbortWithError(500, errors.New("GinJWT does not exist in context"))
// 			return
// 		}

// 		ginJWT, isOk := ginJWTInterface.(GinJWT)
// 		if isOk {
// 			c.AbortWithError(500, errors.New("GinJWT is not of type GinJWT"))
// 			return
// 		}
// 		//if we have got to this point ginJWT contains a instance of GinJWT

// 		obj, err := jose.ParseSigned(c.GetHeader(ginJWT.Header))
// 		if err != nil {
// 			c.Error(err)
// 		}
// 		plain, err := obj.Verify(ginJWT.JWK)
// 		if err != nil {
// 			c.Error(err)
// 		}
// 		c.Set("Auth_Plain", plain)
// 		c.Next()
// 	}
// }
