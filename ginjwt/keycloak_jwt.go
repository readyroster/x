package ginjwt

import (
	"encoding/json"
	"errors"

	"github.com/gin-gonic/gin"
)

//JWT represents the common fields defined in RFC7519
type JWT struct {
	Issuer      string `json:"iss"`
	Subject     string `json:"sub"`
	Audience    string `json:"aud"`
	ExpiryTime  int64  `json:"exp"`
	NotBefore   int64  `json:"nbf"`
	IssuedAt    int64  `json:"iat"`
	JWTID       string `json:"jti"`
	Type        string `json:"typ"`
	ContentType string `json:"cty"`
}

func GetJSONWebTokenFromContext(c *gin.Context) (*JWT, error) {
	bytes, bytesExist := c.MustGet("JWT").([]byte)
	if !bytesExist {
		return nil, errors.New("JWT in context was not of type byte array")

	}
	var jwt JWT
	err := json.Unmarshal(bytes, &jwt)
	return &jwt, err
}

func GetKeycloakJWTFromContext(c *gin.Context) (*KeycloakJWT, error) {
	bytes, bytesExist := c.MustGet("JWT").([]byte)
	if !bytesExist {
		return nil, errors.New("JWT in context was not of type byte array")

	}
	var keycloak KeycloakJWT
	err := json.Unmarshal(bytes, &keycloak)
	return &keycloak, err
}

//KeycloakJWT represemts a JWT provided by keycloak
type KeycloakJWT struct {
	JWT
	AuthorizedParty      string                    `json:"azp,omitempty"`
	AutheticationContext string                    `json:"acr,omitempty"`
	AuthenticateTime     int                       `json:"auth_time,omitempty"`
	AllowedOrigins       []string                  `json:"allowed-origins,omitempty"`
	RealmAccess          KeycloakJWTRealmAccess    `json:"realm_access,omitempty"`
	ResourceAccess       KeycloakJWTResourceAccess `json:"resource_access,omitempty"`
	Scope                string                    `json:"scope,omitempty"`
	EmailVerified        bool                      `json:"email_verified,omitempty"`
	Name                 string                    `json:"name,omitempty"`
	PreferredUsername    string                    `json:"preferred_username,omitempty"`
	GivenName            string                    `json:"given_name,omitempty"`
	FamilyName           string                    `json:"family_name,omitempty"`
	Email                string                    `json:"email,omitempty"`
}

//KeycloakJWTRealmAccess represents a realm access from keycloak
type KeycloakJWTRealmAccess struct {
	Roles []string `json:"roles"`
}

//KeycloakJWTResourceAccess represents a resource access
type KeycloakJWTResourceAccess struct {
	Account KeycloakJWTResourceAccessAccount `json:"account"`
}

//KeycloakJWTResourceAccessAccount represents a account in a access
type KeycloakJWTResourceAccessAccount struct {
	Roles []string `json:"roles"`
}
