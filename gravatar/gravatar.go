package gravatar

import (
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

const (
	//GravatarAPIURL is the url we use to contace gravatar
	GravatarAPIURL string = "https://en.gravatar.com/"
)

//Profile is a profile as provided by gravatar
type Profile struct {
	ID                int               `json:"id"`
	Hash              string            `json:"hash"`
	RequestHash       string            `json:"requestHash"`
	ProfileURL        string            `json:"profileUrl"`
	PreferredUsername string            `json:"preferredUsername"`
	ThumbnailURL      string            `json:"thumbnailUrl"`
	Photos            []gravatarPhoto   `json:"photos"`
	Name              gravatarName      `json:"name"`
	DisplayName       string            `json:"displayName"`
	CurrentLocation   string            `json:"currentLocation"`
	Emails            []gravatarEmail   `json:"emails"`
	Accounts          []gravatarAccount `json:"accounts"`
	URLs              []gravatarURL     `json:"urls"`
}
type gravatarPhoto struct {
	Value string `json:"value"`
	Type  string `json:"type"`
}

type gravatarName struct {
	GivenName  string `json:"givenName"`
	FamilyName string `json:"familyName"`
	Formatted  string `json:"formatted"`
}

type gravatarEmail struct {
	Primary bool   `json:"primary"`
	Value   string `json:"value"`
}

type gravatarAccount struct {
	Domain    string `json:"domain"`
	Display   string `json:"display"`
	URL       string `json:"url"`
	UserID    string `json:"userid"`
	Verfified bool   `json:"verified"`
	ShortName string `json:"shortname"`
}

type gravatarURL struct {
	Value string `json:"value"`
	Title string `json:"title"`
}

func generateEmailHash(email string) string {
	hasher := md5.New()
	hasher.Write([]byte(email))
	return hex.EncodeToString(hasher.Sum(nil))
}

//GetGravatarProfile is used to get a profile from a email
func GetGravatarProfile(email string) (*Profile, error) {
	hash := generateEmailHash(email)

	resp, err := http.Get(fmt.Sprintf("https://en.gravatar.com/%s.json", hash))

	if err != nil {
		return nil, err
	}
	if resp.Body != nil {
		bytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}
		var profile Profile
		err = json.Unmarshal(bytes, &profile)
		if err != nil {
			return nil, err
		}
		return &profile, nil
	}
	return nil, nil
}

// //Gravatar represents a gravatar api instance
// type Gravatar struct {
// 	md5Hash string
// }

// func NewGravatar(email string) *Gravatar {
// 	hasher := md5.New()
// 	hasher.Write([]byte(email))
// 	hash := hex.EncodeToString(hasher.Sum(nil))

// 	return &Gravatar{md5Hash: hash}
// }
