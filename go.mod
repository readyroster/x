module gitlab.com/readyroster/x.git

go 1.13

require (
	github.com/gin-gonic/gin v1.6.1
	github.com/go-pg/pg v8.0.6+incompatible
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/rs/xid v1.2.1
	github.com/rs/zerolog v1.18.0
	github.com/zenazn/goji v0.9.0
	gopkg.in/square/go-jose.v2 v2.4.1
	mellium.im/sasl v0.2.1 // indirect
)
